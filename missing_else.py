from logilab import astng

from pylint.interfaces import IASTNGChecker
from pylint.checkers import BaseChecker

class MissingElseChecker(BaseChecker):
    """
    Checks for if blocks that don't have an else clause
    """
    
    __implements__ = IASTNGChecker

    name = 'missing_else'
    msgs = {
        'W9901': ('if with no else clause',
                  "There's an if block (with no elif) with no else clause"),
        'W9902': ('if/elif with no else clause',
                  "There's an if/elif set of blocks with no corresponding else clause"),
        }
    options = (
               ('warn_if_no_else',
                   {'default': False, 'type': 'yn', 'metavar': '<y_or_n>',
                    'help': 'Warn about missing else clause when there is an if (but no elif)'}
                   ),
               ('warn_if_elif_no_else',
                   {'default': True, 'type': 'yn', 'metavar': '<y_or_n>',
                    'help': 'Warn about if/elif blocks that have no corresponding else clause'}
                   )
               )
 
    # this is important so that your checker is executed before others
    priority = -1 

    def visit_if(self, node):
        assert len(node.getChildren()) % 2 == 1, "There are an even amount of nodes for "+repr(node)

       # True if there is an else clause on this if block
        has_else = node.getChildren()[-1] is not None

        # True if there are elif methods
        has_elifs = len(node.getChildren()) > 3

        if self.config.warn_if_no_else and not has_else:
            self.add_message('W9901', line=node.lineno)

        if self.config.warn_if_elif_no_else and has_elifs and not has_else:
            self.add_message('W9902', line=node.lineno)


    
def register(linter):
    """required method to auto register this checker"""
    linter.register_checker(MissingElseChecker(linter))
        
